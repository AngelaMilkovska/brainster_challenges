import "./App.css";
import SignInPage from "./pages/SignInPage";
import SignUpPage from "./pages/SignUpPage";
import Overview from "./pages/Overview";
import Categories from "./pages/Categories";
import Statistics from "./pages/Statistics";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Container from "@mui/material/Container";
import WizardBudget from "./components/WizardBudget";
import { Provider } from "./context/Context";
import SelectedCategoriesPage from "./pages/SelectedCategoriesPage";
import ExpencePercategoryPage from "./pages/ExpencePercategoryPage";

function App() {
  return (
    <div className="App">
      <Provider>
        <Router>
          <Container maxWidth="lg" sx={[{ paddingX: 3 }]}>
            {/* <Logo /> */}
            <Routes>
              <Route path="/" element={<SignInPage />} />
              <Route path="/SignUpPage/" element={<SignUpPage />} />
              <Route path="/Overview/" element={<Overview />} />
              <Route path="/WizardAmountPage/" element={<WizardBudget />} />
              <Route
                path="/SelectCategories/"
                element={<SelectedCategoriesPage />}
              />
              <Route
                path="/CategoryExpense/"
                element={<ExpencePercategoryPage />}
              />
              <Route path="/Categories/" element={<Categories />} />
              <Route path="/Statistics/" element={<Statistics />} />
            </Routes>
          </Container>
        </Router>
      </Provider>
    </div>
  );
}

export default App;

