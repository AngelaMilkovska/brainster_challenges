import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import Box from "@mui/material/Box";
import LoginAccount from "../components/LoginAccount";
import Container from "@mui/material/Container";

export default function SignInPage() {
  return (
    <Container>
      <Box>
        <LoginAccount />
      </Box>
    </Container>
  );
}
