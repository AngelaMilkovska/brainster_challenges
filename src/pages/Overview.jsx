import {
    Box,
    Button,
    Dialog,
    Fab,
    Icon,
    List,
    ListItem,
    ListItemIcon,
    ListItemText,
    Menu,
    MenuItem,
    Paper,
    Typography,
  } from "@mui/material";
  import React, { useContext, useEffect, useState } from "react";
  import MenuBottom from "../components/MenuBottom";
  import MenuComponent from "../components/MenuComponent";
  import { Context } from "../context/Context";
  import AddEditEntry from "../components/AddEditEntry";
  import AddIcon from "@mui/icons-material/Add";
  import styled from "@emotion/styled";
  
  const StyledBox = styled(Box)`
    /* background-color: ${(props) => (props.opacity ? "red" : "green")}; */
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    /* background-color: rgba(255, 255, 255, 0.75); */
  `;
  
  const StyledModal = styled(Dialog)`
    .MuiBackdrop-root.css-yiavyu-MuiBackdrop-root-MuiDialog-backdrop {
      background-color: rgba(255, 255, 255, 0.5);
    }
  `;
  
  const StyledIncomeBtn = styled(Button)`
    position: fixed;
    right: 10px;
    bottom: 170px;
  `;
  
  const StyledExpenseBtn = styled(Button)`
    position: fixed;
    right: 10px;
    bottom: 110px;
  `;
  const StyledFab = styled(Fab)`
    position: fixed;
    bottom: 40px;
    right: 10px;
    background-color: #09f7e7;
    color: black;
  
    @media (min-width: 425px) {
      right: 25px;
    } ;
  `;
  
  export default function Overview() {
    const { dataAPI, categories, setCategories, entryTotal, currentCategoryId } =
      useContext(Context);
  
    // add/edit modal
    const [isModalOpen, setIsModalOpen] = useState(false);
    // show/hide add income/expense buttons
    const [isBtnShown, setIsBtnShown] = useState(false);
    // stores selected entry
    const [selectedEntry, setSelectedEntry] = useState("");
    // right click menu
    const [contextMenu, setContextMenu] = useState(null);
  
    // taking from LS
    useEffect(() => {
      if (localStorage.getItem("categories")) {
        setCategories(JSON.parse(localStorage.getItem("categories")));
      } else {
        setCategories(categories);
      }
    }, []);
  
    // close the AddEdit Entries modal
    const handleClose = () => {
      setIsModalOpen(false);
    };
  
    // handle right click on entries
    const handleContextMenu = (event) => {
      event.preventDefault();
      setContextMenu(
        contextMenu === null
          ? {
              mouseX: event.clientX - 2,
              mouseY: event.clientY - 4,
            }
          : // repeated contextmenu when it is already open closes it with Chrome 84 on Ubuntu
            // Other native context menus might behave different.
            // With this behavior we prevent contextmenu from the backdrop to re-locale existing context menus.
            null
      );
    };
    // close context menu (right click)
    const handleCloseMenu = () => {
      setContextMenu(null);
    };
  
    return (
      <>
        <StyledBox> </StyledBox>
        {/* navbar component */}
        <MenuComponent title={"Overview"} dataAPI={dataAPI} />
        {/* income box  */}
        <Paper elevation={6} sx={{ marginTop: "25px" }}>
          <Typography
            variant="body1"
            component="h5"
            sx={{
              textAlign: "left",
              backgroundColor: "#EBEBEB",
              color: "gray",
              padding: "6px",
            }}
          >
            Income
          </Typography>
          <List>
            {categories.map((eachCategory) => {
              return eachCategory.isEnabled && eachCategory.type === "income" ? (
                <ListItem key={eachCategory.id}>
                  <ListItemIcon>
                    <Icon>{eachCategory.icon}</Icon>
                  </ListItemIcon>
                  <ListItemText primary={eachCategory.name} />
                  <span>
                    {entryTotal}/{eachCategory.budget}
                  </span>
                </ListItem>
              ) : (
                ""
              );
            })}
          </List>
        </Paper>
        {/* expense box */}
        <Paper elevation={6} sx={{ marginTop: "25px" }}>
          <Typography
            variant="body1"
            component="h5"
            sx={{
              textAlign: "left",
              backgroundColor: "#EBEBEB",
              color: "gray",
              padding: "6px",
            }}
          >
            Expense
          </Typography>
          <List>
            {categories.map((eachCategory) => {
              return eachCategory.isEnabled && eachCategory.type === "expense" ? (
                <ListItem key={eachCategory.id}>
                  <ListItemIcon>
                    <Icon>{eachCategory.icon}</Icon>
                  </ListItemIcon>
                  <ListItemText primary={eachCategory.name} />
                  <span>
                    {entryTotal}/{eachCategory.budget}
                  </span>
                </ListItem>
              ) : (
                ""
              );
            })}
          </List>
        </Paper>
        {/* entries box */}
        <Paper elevation={6} sx={{ marginTop: "25px" }}>
          <Typography
            variant="body1"
            component="h5"
            sx={{
              textAlign: "left",
              backgroundColor: "#EBEBEB",
              color: "gray",
              padding: "6px",
            }}
          >
            Entries
          </Typography>
  
          {categories?.map((category) => {
            return category?.entries.map((entry) => {
              return (
                <List
                  key={entry.id}
                  onClick={() => {
                    setIsModalOpen(true);
                    setSelectedEntry(entry);
                  }}
                  onContextMenu={handleContextMenu}
                  style={{ cursor: "context-menu" }}
                >
                  <ListItem>
                    <ListItemIcon>
                      <Icon>{category.icon}</Icon>
                    </ListItemIcon>
  
                    <ListItemText primary={entry.description} />
                    <span>{entry.amount}</span>
                  </ListItem>
                </List>
              );
            });
          })}
          {/* right click menu */}
  
          <Menu
            open={contextMenu !== null}
            onClose={handleClose}
            anchorReference="anchorPosition"
            anchorPosition={
              contextMenu !== null
                ? { top: contextMenu.mouseY, left: contextMenu.mouseX }
                : undefined
            }
          >
            <MenuItem onClick={handleCloseMenu}>Duplicate</MenuItem>
            <MenuItem onClick={handleCloseMenu}>Create New</MenuItem>
            <MenuItem onClick={handleCloseMenu}>Delete</MenuItem>
          </Menu>
        </Paper>
        {/* bottom menu */}
        <StyledModal open={isModalOpen} onClose={handleClose}>
          <AddEditEntry
            // takes clicked entry
            entry={selectedEntry}
            onClose={handleClose}
          />
        </StyledModal>
  
        <MenuBottom />
        {isBtnShown && (
          <>
            <StyledIncomeBtn
              type="button"
              variant="contained"
              onClick={() => setIsModalOpen(true)}
            >
              Add Expense
            </StyledIncomeBtn>
            <StyledExpenseBtn
              type="button"
              variant="contained"
              onClick={() => setIsModalOpen(true)}
            >
              Add Income
            </StyledExpenseBtn>
          </>
        )}
  
        <StyledFab
          color="primary"
          aria-label="add"
          onClick={() => setIsBtnShown(!isBtnShown)}
        >
          <AddIcon />
        </StyledFab>
      </>
    );
  }
  