import Box from "@mui/material/Box";
import Container from "@mui/material/Container";
import React from "react";
import ExpencePerCategoryComponent from "../components/ExpencePerCategoryComponent";

export default function ExpencePercategoryPage() {
  return (
    <Container>
      <Box>
        <ExpencePerCategoryComponent />
      </Box>
    </Container>
  );
}
