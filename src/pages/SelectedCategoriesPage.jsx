import Box from "@mui/material/Box";
import Container from "@mui/material/Container";
import React from "react";
import SelectInitalCategories from "../components/SelectInitalCategories";

export default function SelectedCategoriesPage() {
  return (
    <Container>
      <Box>
        <SelectInitalCategories />
      </Box>
    </Container>
  );
}
