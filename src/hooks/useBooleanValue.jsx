import React, { useState } from "react";

export default function useBooleanValue(props) {
  const [value, setValue] = useState(props);

  const handleBooleanValue = () => {
    setValue(!value);
  };

  return [value, handleBooleanValue];
}
