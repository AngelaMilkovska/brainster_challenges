import { useState } from "react";

export default function useSetUpPage() {
  const [value, setValue] = useState("");

  const handleValue = (e) => {
    setValue(e.target.value);
  };

  const handleResetValue = () => {
    setValue("");
  };

  return [value, handleValue, handleResetValue];
}
