import React from "react";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";

export default function Footer() {
  return (
    <Box marginTop={10} marginBottom={4}>
      <Typography fontSize={13} sx={[{ color: "#828b9f" }]}>
        Your money leavs footprints whenever they come and go!
      </Typography>

      <Typography fontSize={14} sx={[{ color: "#652fe2" }]}>
        Track them with Coin Tracker
      </Typography>
    </Box>
  );
}
