import styled from "@emotion/styled";
import {
  Button,
  Checkbox,
  DialogActions,
  DialogContent,
  DialogTitle,
  FormControl,
  FormControlLabel,
  FormGroup,
  Icon,
  InputLabel,
  MenuItem,
  Select,
  TextField,
} from "@mui/material";
import React, { useContext, useState } from "react";
import { Context } from "../context/Context";
import { Icons } from "./Exports";

// styled components
const StyledCheckboxLabel = styled(FormControlLabel)`
  .css-ahj2mt-MuiTypography-root {
    flex-grow: 1;
  }
  .MuiFormControlLabel-root.css-11nowet-MuiFormControlLabel-root {
    margin-left: 0 !important;
    margin-right: 0 !important;
  }
  .css-12wnr2w-MuiButtonBase-root-MuiCheckbox-root {
    /* margin-right: 11px; */
  }
`;

export default function AddEditCategory({ onClose, category }) {
  const { categories, addCategory, updateCategory } = useContext(Context);

  // if there is id in category it is converted into boolean value
  const isEditing = !!category?.id;
  const [categoryData, setCategoryData] = useState(
    category || {
      id: undefined,
      type: "",
      name: "",
      budget: undefined,
      icon: "",
      isEnabled: undefined,
      entries: [],
    }
  );
  // icons from initial categories in Context
  const iconsFromInitialCategories = categories.map(
    (eachCategory) => eachCategory.icon
  );
  // all category icons
  const allIcons = [...Icons, ...iconsFromInitialCategories];

  // to avoid duplicates of icon names and have only unique icon names
  const allUniqueIcons = [...new Set(allIcons)];

  return (
    <>
      <DialogTitle>{isEditing ? "Update" : "Add New Category"}</DialogTitle>
      <form
        action=""
        onSubmit={(e) => {
          e.preventDefault();

          isEditing ? updateCategory(categoryData) : addCategory(categoryData);

          // closing the modal after submit
          onClose();
        }}
      >
        <DialogContent>
          <FormControl fullWidth sx={{ mt: "25px" }}>
            <InputLabel id="demo-simple-select-label">
              Type of category
            </InputLabel>
            <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              value={categoryData?.type}
              label="Type of category"
              onChange={(e) => {
                setCategoryData({ ...categoryData, type: e.target.value });
              }}
            >
              <MenuItem value={"income"}>Income</MenuItem>
              <MenuItem value={"expense"}>Expense</MenuItem>
            </Select>
          </FormControl>
          <TextField
            type="text"
            id="name"
            label="Name"
            fullWidth
            variant="outlined"
            sx={{ mt: "35px" }}
            value={categoryData?.name}
            onChange={(e) => {
              setCategoryData({ ...categoryData, name: e.target.value });
            }}
          />
          <FormControl fullWidth sx={{ mt: "35px" }}>
            <InputLabel id="demo-simple-select-label">Icon</InputLabel>
            <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              value={categoryData?.icon}
              label="Icon"
              onChange={(e) => {
                setCategoryData({ ...categoryData, icon: e.target.value });
              }}
            >
              {allUniqueIcons.map((icon) => (
                <MenuItem key={icon} value={`${icon}`}>
                  <Icon>{icon}</Icon>
                  {icon}
                </MenuItem>
              ))}
            </Select>
          </FormControl>
          <TextField
            type="number"
            id="budget"
            label="Budget"
            fullWidth
            variant="outlined"
            sx={{ mt: "35px" }}
            value={categoryData?.budget}
            onChange={(e) => {
              setCategoryData({ ...categoryData, budget: e.target.value });
            }}
          />

          <FormGroup>
            <StyledCheckboxLabel
              value="start"
              control={<Checkbox checked={categoryData.isEnabled} />}
              label="Enabled"
              labelPlacement="start"
              onChange={(e) => {
                setCategoryData({
                  ...categoryData,
                  isEnabled: e.target.checked,
                });
              }}
            />
          </FormGroup>
        </DialogContent>

        <DialogActions>
          <Button onClick={onClose}>Cancel</Button>
          <Button type="submit">{isEditing ? "Update" : "Add"}</Button>
        </DialogActions>
      </form>
    </>
  );
}
