import {
    AppBar,
    Avatar,
    Container,
    IconButton,
    ThemeProvider,
    Toolbar,
    Typography,
  } from "@mui/material";
  import React, { useState } from "react";
  import { createTheme } from "@mui/material";
  import Menu from "@mui/material/Menu";
  import MenuItem from "@mui/material/MenuItem";
  import { useNavigate } from "react-router-dom";
  
  export default function MenuComponent(props) {
    const [anchorEl, setAnchorEl] = useState(null);
    const navigate = useNavigate();
  
    const handleMenu = (event) => {
      setAnchorEl(event.currentTarget);
    };
  
    // on sign out
    const handleClose = () => {
      localStorage.clear();
      setAnchorEl(null);
      navigate("/");
    };
  
    const theme = createTheme({
      components: {
        MuiTypography: {
          variants: [
            {
              props: { variant: "h6" },
              style: { textAlign: "left", marginLeft: "15px" },
            },
          ],
        },
      },
    });
    return (
      <ThemeProvider theme={theme}>
        <AppBar position="fixed" sx={{ bgcolor: "#652fe2" }}>
          <Toolbar>
            <img src="/images/Logo.png" className="logoStyleMenu" />
  
            <Typography
              variant="h6"
              component="span"
              alignLeft
              sx={{ flexGrow: 1 }}
            >
              {props.title}
            </Typography>
            <IconButton
              size="large"
              aria-label="account of current user"
              aria-controls="menu-appbar"
              aria-haspopup="true"
              onClick={handleMenu}
              color="inherit"
            >
              <Avatar src={props.dataAPI?.picture?.medium} />
            </IconButton>
            <Menu
              id="menu-appbar"
              anchorEl={anchorEl}
              anchorOrigin={{
                vertical: "top",
                horizontal: "right",
              }}
              keepMounted
              transformOrigin={{
                vertical: "top",
                horizontal: "right",
              }}
              open={Boolean(anchorEl)}
              onClose={handleClose}
            >
              <MenuItem onClick={handleClose}>Sign Out</MenuItem>
            </Menu>
          </Toolbar>
        </AppBar>
      </ThemeProvider>
    );
  }
  