import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";
import React from "react";
import { Link } from "react-router-dom";

export default function LoginText(props) {
  return (
    <Box marginTop={4}>
      <Typography component="p" sx={[{ color: "#828b9f" }]} fontSize={16}>
        {props.content}
      </Typography>
      <Typography>
        <Link to={props.path}>{props.linkTitle}</Link>
      </Typography>
    </Box>
  );
}
