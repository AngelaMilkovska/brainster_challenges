import React from "react";
import Typography from "@mui/material/Typography";

export default function HomeTitle(props) {
  return (
    <Typography variant="h5" component="h1" className="titileStyle">
      {props.title}
    </Typography>
  );
}
