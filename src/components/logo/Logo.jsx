import { Grid } from "@mui/material";
import React from "react";

export default function Logo() {
  return (
    <Grid item xs={12} md={6}>
      <img src="/images/LogoCompact.svg" alt="" className="logoStyle" />
    </Grid>
  );
}
