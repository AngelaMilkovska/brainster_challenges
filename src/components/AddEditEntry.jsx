import styled from "@emotion/styled";
import {
  Button,
  Checkbox,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  FormControl,
  FormControlLabel,
  FormGroup,
  Icon,
  InputLabel,
  MenuItem,
  Select,
  TextField,
} from "@mui/material";
import { Box } from "@mui/system";
import React, { useContext, useState, useEffect } from "react";
import { Context } from "../context/Context";
import { Icons } from "./Exports";

// styled components
const StyledCheckboxLabel = styled(FormControlLabel)`
  .css-ahj2mt-MuiTypography-root {
    flex-grow: 1;
  }
  .MuiFormControlLabel-root.css-11nowet-MuiFormControlLabel-root {
    margin-left: 0 !important;
    margin-right: 0 !important;
  }
  .css-12wnr2w-MuiButtonBase-root-MuiCheckbox-root {
    /* margin-right: 11px; */
  }
`;

export default function AddEditEntry({ onClose, entry }) {
  const {
    categories,
    addEntry,
    updateEntry,

    setCurrentCategoryId,
  } = useContext(Context);

  // if there is id in entry it is converted into boolean value
  const isEditing = !!entry?.id;

  // category type value
  const [categoryType, setCategoryType] = useState("income");
  // category name value
  const [categoryName, setCategoryName] = useState(" ");

  const [entryData, setEntryData] = useState(
    entry || {
      id: undefined,
      amount: null,
      date: " ",
      description: " ",
    }
  );
  // taking the id of the selected category
  useEffect(() => {
    categories.map((category) => {
      if (category.name === categoryName) {
        setCurrentCategoryId(category.id);
      }
    });
  }, [categoryName]);

  return (
    <>
      <DialogTitle>{`${isEditing ? "Update" : "Add New "} Entry`}</DialogTitle>
      <form
        action=""
        onSubmit={(e) => {
          e.preventDefault();

          isEditing ? updateEntry(entryData) : addEntry(entryData);

          // closing the modal after submit
          onClose();
        }}
      >
        <DialogContent>
          {/* Type of entry */}
          <FormControl fullWidth sx={{ mt: "25px" }}>
            <InputLabel>Type</InputLabel>
            <Select
              label="Type"
              value={categoryType}
              onChange={(e) => {
                setCategoryType(e.target.value);
              }}
            >
              <MenuItem value={"income"}>Income</MenuItem>
              <MenuItem value={"expense"}>Expense</MenuItem>
            </Select>
          </FormControl>
          {/* Select Category */}
          <FormControl fullWidth sx={{ mt: "25px" }}>
            <InputLabel>Category</InputLabel>
            <Select
              label="Category"
              value={categoryName}
              onChange={(e) => {
                setCategoryName(e.target.value);
              }}
            >
              {categories.map((category) => {
                return (
                  category.type === categoryType && (
                    <MenuItem key={category.name} value={category.name}>
                      {category.name}
                    </MenuItem>
                  )
                );
              })}
            </Select>
          </FormControl>
          <TextField
            type="number"
            id="amount"
            label="Amount"
            fullWidth
            variant="outlined"
            sx={{ mt: "35px" }}
            value={entryData.amount}
            onChange={(e) => {
              setEntryData({ ...entryData, amount: parseInt(e.target.value) });
            }}
          />

          <TextField
            type="date"
            id="date"
            label="Date"
            fullWidth
            variant="outlined"
            sx={{ mt: "35px" }}
            value={entryData.date}
            onChange={(e) => {
              setEntryData({ ...entryData, date: e.target.value });
            }}
          />
          <TextField
            type="text"
            id="description"
            label="Description"
            fullWidth
            variant="outlined"
            sx={{ mt: "35px" }}
            value={entryData.description}
            onChange={(e) => {
              setEntryData({ ...entryData, description: e.target.value });
            }}
          />
        </DialogContent>

        <DialogActions>
          <Button onClick={onClose}>Cancel</Button>
          <Button type="submit">{isEditing ? "Update" : "Add"}</Button>
        </DialogActions>
      </form>
    </>
  );
}
