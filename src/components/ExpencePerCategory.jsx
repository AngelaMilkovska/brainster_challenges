import {
    Icon,
    Input,
    InputAdornment,
    List,
    ListItem,
    ListItemButton,
    ListItemIcon,
    ListItemText,
    TextField,
    Typography,
  } from "@mui/material";
  import React, { useContext, useEffect } from "react";
  import { useNavigate } from "react-router-dom";
  import { Context } from "../context/Context";
  import ButtonComponent from "./ButtonComponent";
  
  export default function ExpencePerCategory() {
    const { categories, setCategories } = useContext(Context);
    const navigate = useNavigate();
  
    useEffect(() => {
      if (localStorage.getItem("categories")) {
        setCategories(JSON.parse(localStorage.getItem("categories")));
      } else {
        setCategories(categories);
      }
    }, []);
  
    return (
      <>
        <List sx={{ marginTop: "30px" }}>
          {categories.map((category) =>
            category.isEnabled ? (
              <ListItem key={category.id} divider>
                <ListItemIcon>
                  <Icon>{category.icon}</Icon>
                </ListItemIcon>
                <ListItemButton>
                  <ListItemText id={category.id} primary={category.name} />
                </ListItemButton>
                <Input
                  type="number"
                  margin={"none"}
                  id="input-with-icon-textfield"
                  variant="standard"
                  value={category.budget}
                  onChange={(e) => {
                    let newCategories = categories.slice();
                    newCategories.map((newCategory) => {
                      if (category.id === newCategory.id) {
                        category.budget = e.target.value;
                        localStorage.setItem(
                          "categories",
                          JSON.stringify(categories)
                        );
                      }
                      setCategories(categories);
                    });
                  }}
                ></Input>
              </ListItem>
            ) : (
              ""
            )
          )}
        </List>
  
        <ButtonComponent
          text={"COMPLETE"}
          width={"100%"}
          marginTop={35}
          color={"white"}
          backgroundColor={"#652fe2"}
          handleSubmit={() => {
            navigate("/Overview/");
          }}
        />
      </>
    );
  }
  