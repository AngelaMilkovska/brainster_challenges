import React from "react";
import InputLabel from "@mui/material/InputLabel";
import FormControl from "@mui/material/FormControl";
import OutlinedInput from "@mui/material/OutlinedInput";
import InputAdornment from "@mui/material/InputAdornment";
import IconButton from "@mui/material/IconButton";
import Typography from "@mui/material/Typography";
import VisibilityOff from "@mui/icons-material/VisibilityOff";
import Visibility from "@mui/icons-material/Visibility";

export default function PasswordInput(props) {
  return (
    <FormControl fullWidth variant="outlined" sx={[{ marginTop: 4 }]}>
      <InputLabel htmlFor="outlined-adornment-password">Password</InputLabel>
      <OutlinedInput
        error={props.error}
        id="outlined-adornment-password"
        type={props.isShownPassword ? "text" : "password"}
        value={props.value}
        onChange={props.onChange}
        endAdornment={
          <InputAdornment position="end">
            <IconButton
              aria-label="toggle password visibility"
              onClick={props.onClick}
              edge="end"
            >
              {props.password ? <VisibilityOff /> : <Visibility />}
            </IconButton>
          </InputAdornment>
        }
        label="Password"
      />
      {props.error === true && (
        <Typography
          variant="h6"
          component="p"
          className="validationParPassword"
        >
          {props.validationFeedbackPassword}
        </Typography>
      )}
    </FormControl>
  );
}
